﻿namespace WindowsFormsApp1
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.button0 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button19 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.button30 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.label0 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.button_0xFFFFFFFF = new System.Windows.Forms.Button();
            this.button_0x00000000 = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label32 = new System.Windows.Forms.Label();
            this.textBox_hex = new System.Windows.Forms.TextBox();
            this.textBox_dec = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button0
            // 
            this.button0.Location = new System.Drawing.Point(527, 98);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(22, 32);
            this.button0.TabIndex = 0;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(497, 98);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(22, 32);
            this.button1.TabIndex = 1;
            this.button1.Text = "0";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(467, 98);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(22, 32);
            this.button2.TabIndex = 2;
            this.button2.Text = "0";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(437, 98);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(22, 32);
            this.button3.TabIndex = 3;
            this.button3.Text = "0";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(387, 98);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(22, 32);
            this.button4.TabIndex = 4;
            this.button4.Text = "0";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(357, 98);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(22, 32);
            this.button5.TabIndex = 5;
            this.button5.Text = "0";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(327, 98);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(22, 32);
            this.button6.TabIndex = 6;
            this.button6.Text = "0";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(297, 98);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(22, 32);
            this.button7.TabIndex = 7;
            this.button7.Text = "0";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(247, 98);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(22, 32);
            this.button8.TabIndex = 8;
            this.button8.Text = "0";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(217, 98);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(22, 32);
            this.button9.TabIndex = 9;
            this.button9.Text = "0";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(187, 98);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(22, 32);
            this.button10.TabIndex = 10;
            this.button10.Text = "0";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(157, 98);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(22, 32);
            this.button11.TabIndex = 11;
            this.button11.Text = "0";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(107, 98);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(22, 32);
            this.button12.TabIndex = 12;
            this.button12.Text = "0";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(77, 98);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(22, 32);
            this.button13.TabIndex = 13;
            this.button13.Text = "0";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(47, 98);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(22, 32);
            this.button14.TabIndex = 14;
            this.button14.Text = "0";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(17, 98);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(22, 32);
            this.button15.TabIndex = 15;
            this.button15.Text = "0";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(527, 23);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(22, 32);
            this.button16.TabIndex = 16;
            this.button16.Text = "0";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(497, 23);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(22, 32);
            this.button17.TabIndex = 17;
            this.button17.Text = "0";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(467, 23);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(22, 32);
            this.button18.TabIndex = 18;
            this.button18.Text = "0";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(437, 23);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(22, 32);
            this.button19.TabIndex = 19;
            this.button19.Text = "0";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(387, 23);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(22, 32);
            this.button20.TabIndex = 20;
            this.button20.Text = "0";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(357, 23);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(22, 32);
            this.button21.TabIndex = 21;
            this.button21.Text = "0";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(327, 23);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(22, 32);
            this.button22.TabIndex = 22;
            this.button22.Text = "0";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(297, 23);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(22, 32);
            this.button23.TabIndex = 23;
            this.button23.Text = "0";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(247, 23);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(22, 32);
            this.button24.TabIndex = 24;
            this.button24.Text = "0";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(217, 23);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(22, 32);
            this.button25.TabIndex = 25;
            this.button25.Text = "0";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(187, 23);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(22, 32);
            this.button26.TabIndex = 26;
            this.button26.Text = "0";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(157, 23);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(22, 32);
            this.button27.TabIndex = 27;
            this.button27.Text = "0";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(107, 23);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(22, 32);
            this.button28.TabIndex = 28;
            this.button28.Text = "0";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(77, 23);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(22, 32);
            this.button29.TabIndex = 29;
            this.button29.Text = "0";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(47, 23);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(22, 32);
            this.button30.TabIndex = 30;
            this.button30.Text = "0";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(17, 23);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(22, 32);
            this.button31.TabIndex = 31;
            this.button31.Text = "0";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Click += new System.EventHandler(this.buttonBIT_Click);
            // 
            // label0
            // 
            this.label0.AutoSize = true;
            this.label0.Location = new System.Drawing.Point(533, 82);
            this.label0.Name = "label0";
            this.label0.Size = new System.Drawing.Size(11, 12);
            this.label0.TabIndex = 32;
            this.label0.Text = "0";
            this.label0.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(503, 82);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(11, 12);
            this.label1.TabIndex = 33;
            this.label1.Text = "1";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(473, 82);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(11, 12);
            this.label2.TabIndex = 34;
            this.label2.Text = "2";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(443, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(11, 12);
            this.label3.TabIndex = 35;
            this.label3.Text = "3";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(393, 82);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(11, 12);
            this.label4.TabIndex = 36;
            this.label4.Text = "4";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(363, 82);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(11, 12);
            this.label5.TabIndex = 37;
            this.label5.Text = "5";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(333, 82);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(11, 12);
            this.label6.TabIndex = 38;
            this.label6.Text = "6";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(303, 82);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(11, 12);
            this.label7.TabIndex = 39;
            this.label7.Text = "7";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(253, 82);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(11, 12);
            this.label8.TabIndex = 40;
            this.label8.Text = "8";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(226, 82);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(11, 12);
            this.label9.TabIndex = 41;
            this.label9.Text = "9";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(193, 82);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 12);
            this.label10.TabIndex = 42;
            this.label10.Text = "10";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(160, 82);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 12);
            this.label11.TabIndex = 43;
            this.label11.Text = "11";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(110, 82);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(17, 12);
            this.label12.TabIndex = 44;
            this.label12.Text = "12";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(80, 82);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(17, 12);
            this.label13.TabIndex = 45;
            this.label13.Text = "13";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(50, 82);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(17, 12);
            this.label14.TabIndex = 46;
            this.label14.Text = "14";
            this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(20, 82);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(17, 12);
            this.label15.TabIndex = 47;
            this.label15.Text = "15";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(530, 7);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(17, 12);
            this.label16.TabIndex = 48;
            this.label16.Text = "16";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(500, 7);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(17, 12);
            this.label17.TabIndex = 49;
            this.label17.Text = "17";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(470, 7);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(17, 12);
            this.label18.TabIndex = 50;
            this.label18.Text = "18";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(440, 7);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(17, 12);
            this.label19.TabIndex = 51;
            this.label19.Text = "19";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(390, 7);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(17, 12);
            this.label20.TabIndex = 52;
            this.label20.Text = "20";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(360, 7);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(17, 12);
            this.label21.TabIndex = 53;
            this.label21.Text = "21";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(330, 7);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(17, 12);
            this.label22.TabIndex = 54;
            this.label22.Text = "22";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(300, 7);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(17, 12);
            this.label23.TabIndex = 55;
            this.label23.Text = "23";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(250, 7);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(17, 12);
            this.label24.TabIndex = 56;
            this.label24.Text = "24";
            this.label24.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(220, 7);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(17, 12);
            this.label25.TabIndex = 57;
            this.label25.Text = "25";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(190, 7);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(17, 12);
            this.label26.TabIndex = 58;
            this.label26.Text = "26";
            this.label26.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(160, 7);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(17, 12);
            this.label27.TabIndex = 59;
            this.label27.Text = "27";
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(110, 7);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(17, 12);
            this.label28.TabIndex = 60;
            this.label28.Text = "28";
            this.label28.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(80, 7);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(17, 12);
            this.label29.TabIndex = 61;
            this.label29.Text = "29";
            this.label29.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(50, 7);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(17, 12);
            this.label30.TabIndex = 62;
            this.label30.Text = "30";
            this.label30.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(20, 7);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(17, 12);
            this.label31.TabIndex = 63;
            this.label31.Text = "31";
            this.label31.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // button_0xFFFFFFFF
            // 
            this.button_0xFFFFFFFF.Location = new System.Drawing.Point(30, 180);
            this.button_0xFFFFFFFF.Name = "button_0xFFFFFFFF";
            this.button_0xFFFFFFFF.Size = new System.Drawing.Size(75, 23);
            this.button_0xFFFFFFFF.TabIndex = 64;
            this.button_0xFFFFFFFF.Text = "0xFFFFFFFF";
            this.button_0xFFFFFFFF.UseVisualStyleBackColor = true;
            this.button_0xFFFFFFFF.Click += new System.EventHandler(this.button_0xFFFFFFFF_Click);
            // 
            // button_0x00000000
            // 
            this.button_0x00000000.Location = new System.Drawing.Point(170, 180);
            this.button_0x00000000.Name = "button_0x00000000";
            this.button_0x00000000.Size = new System.Drawing.Size(75, 23);
            this.button_0x00000000.TabIndex = 65;
            this.button_0x00000000.Text = "0x00000000";
            this.button_0x00000000.UseVisualStyleBackColor = true;
            this.button_0x00000000.Click += new System.EventHandler(this.button_0x00000000_Click);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.button19);
            this.panel1.Controls.Add(this.button0);
            this.panel1.Controls.Add(this.button1);
            this.panel1.Controls.Add(this.label31);
            this.panel1.Controls.Add(this.button2);
            this.panel1.Controls.Add(this.label30);
            this.panel1.Controls.Add(this.button3);
            this.panel1.Controls.Add(this.label29);
            this.panel1.Controls.Add(this.button4);
            this.panel1.Controls.Add(this.label28);
            this.panel1.Controls.Add(this.button5);
            this.panel1.Controls.Add(this.label27);
            this.panel1.Controls.Add(this.button6);
            this.panel1.Controls.Add(this.label26);
            this.panel1.Controls.Add(this.button7);
            this.panel1.Controls.Add(this.label25);
            this.panel1.Controls.Add(this.button8);
            this.panel1.Controls.Add(this.label24);
            this.panel1.Controls.Add(this.button9);
            this.panel1.Controls.Add(this.label23);
            this.panel1.Controls.Add(this.button10);
            this.panel1.Controls.Add(this.label22);
            this.panel1.Controls.Add(this.button11);
            this.panel1.Controls.Add(this.label21);
            this.panel1.Controls.Add(this.button12);
            this.panel1.Controls.Add(this.label20);
            this.panel1.Controls.Add(this.button13);
            this.panel1.Controls.Add(this.label19);
            this.panel1.Controls.Add(this.button14);
            this.panel1.Controls.Add(this.label18);
            this.panel1.Controls.Add(this.button15);
            this.panel1.Controls.Add(this.label17);
            this.panel1.Controls.Add(this.button16);
            this.panel1.Controls.Add(this.label16);
            this.panel1.Controls.Add(this.button17);
            this.panel1.Controls.Add(this.label15);
            this.panel1.Controls.Add(this.button18);
            this.panel1.Controls.Add(this.label14);
            this.panel1.Controls.Add(this.button20);
            this.panel1.Controls.Add(this.label13);
            this.panel1.Controls.Add(this.button21);
            this.panel1.Controls.Add(this.label12);
            this.panel1.Controls.Add(this.button22);
            this.panel1.Controls.Add(this.label11);
            this.panel1.Controls.Add(this.button23);
            this.panel1.Controls.Add(this.label10);
            this.panel1.Controls.Add(this.button24);
            this.panel1.Controls.Add(this.label9);
            this.panel1.Controls.Add(this.button25);
            this.panel1.Controls.Add(this.label8);
            this.panel1.Controls.Add(this.button26);
            this.panel1.Controls.Add(this.label7);
            this.panel1.Controls.Add(this.button27);
            this.panel1.Controls.Add(this.label6);
            this.panel1.Controls.Add(this.button28);
            this.panel1.Controls.Add(this.label5);
            this.panel1.Controls.Add(this.button29);
            this.panel1.Controls.Add(this.label4);
            this.panel1.Controls.Add(this.button30);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.button31);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label0);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(18, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(575, 141);
            this.panel1.TabIndex = 66;
            this.panel1.Paint += new System.Windows.Forms.PaintEventHandler(this.panel1_Paint);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(7, 235);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(41, 12);
            this.label32.TabIndex = 67;
            this.label32.Text = "16进制";
            // 
            // textBox_hex
            // 
            this.textBox_hex.Location = new System.Drawing.Point(54, 231);
            this.textBox_hex.Name = "textBox_hex";
            this.textBox_hex.Size = new System.Drawing.Size(118, 21);
            this.textBox_hex.TabIndex = 69;
            this.textBox_hex.TextChanged += new System.EventHandler(this.textBox_hex_TextChanged);
            this.textBox_hex.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_hex_KeyPress);
            // 
            // textBox_dec
            // 
            this.textBox_dec.Location = new System.Drawing.Point(336, 231);
            this.textBox_dec.Name = "textBox_dec";
            this.textBox_dec.Size = new System.Drawing.Size(118, 21);
            this.textBox_dec.TabIndex = 71;
            this.textBox_dec.TextChanged += new System.EventHandler(this.textBox_dec_TextChanged);
            this.textBox_dec.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_dec_KeyPress);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(289, 235);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(41, 12);
            this.label33.TabIndex = 70;
            this.label33.Text = "10进制";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(614, 281);
            this.Controls.Add(this.textBox_dec);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.textBox_hex);
            this.Controls.Add(this.label32);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.button_0x00000000);
            this.Controls.Add(this.button_0xFFFFFFFF);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "寄存器配置工具 V1.0";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Label label0;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button button_0xFFFFFFFF;
        private System.Windows.Forms.Button button_0x00000000;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox textBox_hex;
        private System.Windows.Forms.TextBox textBox_dec;
        private System.Windows.Forms.Label label33;
    }
}

