﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;


namespace WindowsFormsApp1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        /* 返回32个按钮值 */
        private UInt32 CalcBitButtonHex()
        {
            UInt32 result = 0;

            foreach (Control c in panel1.Controls)
            {
                if (c is Button)
                {
                    /* 获取按键值, 0或1 */
                    UInt32 bitValue = Convert.ToUInt32(((Button)c).Text);
                    /* 提取字符名字, 0~31 */
                    string str = Regex.Replace(((Button)c).Name, @"[^0-9]+", "");
                    /* 当前位 */
                    Int32 bitNum = Convert.ToInt32(str);

                    result |= (UInt32)bitValue << bitNum;
                }
            }

            return result;
        }

        /* 显示16进制 */
        private void RefreshShow_Hex(UInt32 hex)
        {
            textBox_hex.Text = "0x" + hex.ToString("X8");
        }
        /* 显示10进制 */
        private void RefreshShow_Dec(UInt32 hex)
        {
            textBox_dec.Text = hex.ToString();
        }

        /* 更新二进制初始状态 */
        private void RefreshShow_Bin(UInt32 hex)
        {
            foreach (Control c in panel1.Controls)
            {
                if (c is Button)
                {
                    /* 获取按键值, 0或1 */
                    UInt32 bitValue = Convert.ToUInt32(((Button)c).Text);
                    /* 提取字符名字, 0~31 */
                    string str = Regex.Replace(((Button)c).Name, @"[^0-9]+", "");
                    /* 当前位 */
                    Int32 bitNum = Convert.ToInt32(str);
                    /* 判断当前位的数值 */
                    if ((hex & (1 << bitNum)) > 0) ((Button)c).Text = "1";
                    else ((Button)c).Text = "0";
                }
            }
        }

        /* 翻转按键状态 */
        private void buttonBIT_Click(object sender, EventArgs e)
        {
            if (((Button)sender).Text == "0")
            {
                ((Button)sender).Text = "1";
            }
            else
            {
                ((Button)sender).Text = "0";
            }
            /* 更新16进制显示 */
            RefreshShow_Hex(CalcBitButtonHex());
            /* 更新10进制显示 */
            RefreshShow_Dec(CalcBitButtonHex());
        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {
        }

        /* 将所有位配置为0 */
        private void button_0x00000000_Click(object sender, EventArgs e)
        {
            foreach (Control c in panel1.Controls)
            {
                if (c is Button)
                {
                    ((Button)c).Text = "0";
                }
            }
            /* 更新16进制显示 */
            RefreshShow_Hex(CalcBitButtonHex());
            /* 更新10进制显示 */
            RefreshShow_Dec(CalcBitButtonHex());
        }

        /* 将所有位配置为1 */
        private void button_0xFFFFFFFF_Click(object sender, EventArgs e)
        {
            foreach (Control c in panel1.Controls)
            {
                if (c is Button)
                {
                    ((Button)c).Text = "1";
                }
            }
            /* 更新16进制显示 */
            RefreshShow_Hex(CalcBitButtonHex());
            /* 更新10进制显示 */
            RefreshShow_Dec(CalcBitButtonHex());
        }
        

        /* 互换更新 ********************************************************************************************************/
        private bool HexRef = false;
        private bool DecRef = false;
        private void textBox_hex_KeyPress(object sender, KeyPressEventArgs e)
        {
            DecRef = true;
        }
        private void textBox_dec_KeyPress(object sender, KeyPressEventArgs e)
        {
            HexRef = true;
        }

        private void textBox_hex_TextChanged(object sender, EventArgs e)
        {
            if (DecRef == true)
            {
                DecRef = false;
                try
                {
                    UInt32 Value = Convert.ToUInt32(((TextBox)sender).Text, 16);
                    /* 更新10进制显示 */
                    RefreshShow_Dec(Value);
                    /* 更新二进制 */
                    RefreshShow_Bin(Value);
                }
                catch
                {
                    //MessageBox.Show("数值超出范围或非法", "错误", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                }
            }
        }

        private void textBox_dec_TextChanged(object sender, EventArgs e)
        {
            if (HexRef == true)
            {
                HexRef = false;
                try
                {
                    UInt32 Value = Convert.ToUInt32(((TextBox)sender).Text, 10);
                    /* 更新16进制显示 */
                    RefreshShow_Hex(Value);
                    /* 更新二进制 */
                    RefreshShow_Bin(Value);
                }
                catch
                {
                    //MessageBox.Show("数值超出范围或非法", "错误", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                }
            }
        }
    }
}
